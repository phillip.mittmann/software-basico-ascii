#include <stdio.h>
#include <stdlib.h>
#include <string.h>		// Para usar strings

// SOIL � a biblioteca para leitura das imagens
#include "SOIL.h"

// Um pixel RGB
typedef struct {
    unsigned char r, g, b;
} RGB;

// Uma imagem em RGB
typedef struct {
    int width, height;
    RGB* img;
} Img;

// Prot�tipos
void load(char* name, Img* pic);
int calcValor(char* r, char* g, char* b);
char calcChar(unsigned int v);

// Carrega uma imagem para a struct Img
void load(char* name, Img* pic)
{
    int chan;
    pic->img = (unsigned char*) SOIL_load_image(name, &pic->width, &pic->height, &chan, SOIL_LOAD_RGB);
    if(!pic->img)
    {
        printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
        exit(1);
    }
    printf("Load: %d x %d x %d\n", pic->width, pic->height, chan);
}

int calcValor(char* r, char* g, char* b)
{
    return (0.3 * *r + 0.59 * *g + 0.11 * *b);
}

char calcChar(unsigned int valor)
{
    char caracter;

    if (valor >= 225){
        caracter = '@';
    }
    else if (valor >= 195){
        caracter = '8';
    }
    else if (valor >= 165){
        caracter = 'O';
    }
    else if(valor >= 135){
        caracter = 'C';
    }
     else if (valor >= 105){
        caracter = 'o';
    }
    else if (valor >= 75){
        caracter = 'c';
    }
    else if(valor >= 45){
        caracter = ':';
    }
    else {
        caracter = '.';
    }
    return caracter;
}

int main(int argc, char** argv)
{
    Img pic;
    char imagem[50];
    double reducao;

    printf("Digite o nome da image que deseja carregar: ");
    scanf("%s", imagem);
    strcat(imagem, ".jpg");
    argv[1] = imagem;

    if(argc < 1) {
        printf("loader [img]\n");
        exit(1);
    }
    load(argv[1], &pic);

    //***************************************************************************************************************************************

    /*
    int tam;
    printf("%s", "Digite a quantidade de caracteres a serem utilizadaos: ");
    scanf("%s", &tam);

    char charArray[tam];
    printf("%s", "Digite os caracteres a serem utilizadaos: ");
    scanf("%s", charArray);
    */
    /*
    printf("Digite a reducao dos pixels desejada: ");
    scanf("%lf", &reducao);

    reducao = reducao/100;

    if (reducao == 0)
    {
        reducao = 1;
    }
    */

    reducao = 1;
    int novaAltura = pic.height * reducao;
    int novaLargura = pic.width * reducao;

    char nI[novaAltura][novaLargura];
    int p = 0;

    for (int i=0;i<novaAltura;i++)
    {
        for (int j=0;j<novaLargura;j++)
        {
            int aux = calcValor(&pic.img[p].r, &pic.img[p].g, &pic.img[p].b);
            char c = calcChar(aux);

            nI[i][j] = c;
            p++;
        }
    }

    FILE* html = fopen("imagem.html", "w");
    fprintf(html, "%s\n", "<html><head></head>");
    fprintf(html, "%s\n", "<body style=\"background: black;\" leftmargin=0 topmargin=0> ");
    fprintf(html, "%s\n", "<style>");
    fprintf(html, "%s\n", "pre  {");
    fprintf(html, "%s\n", "color: white;");
    fprintf(html, "%s\n", "font-family: Courier;");
    fprintf(html, "%s\n", "font-size: 8px");
    fprintf(html, "%s\n", "}</style>");
    fprintf(html, "%s\n", "<pre>");

    for(int i=0;i<novaAltura;i++)
    {
        for (int j=0;j<novaLargura;j++)
        {
            if (j == novaLargura-1)
            {
                fprintf(html, "%s", "<br>");
            } else
            {
                fprintf(html, "%c", nI[i][j]);
            }
        }
    }

    fprintf(html, "%s\n", "%s\n", "%s\n", "</pre>",  "</body>", "</html>");
    fclose(html);

    free(pic.img);
}
